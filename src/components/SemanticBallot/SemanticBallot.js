import React, { useEffect, useState } from "react";
import numeral from "numeral";
import ProgressBar from "../ProgressBar/ProgressBar";
import "./SemanticBallot.scss";
import Button from "../Button/Button";

function formatBalanceValue(number, format = "(0.[00]a)") {
  return numeral(number).format(format);
}

const SemanticBallot = (props) => {
  const {
    choices,
    isVoteMode,
    openVoteDialog,
    btnVoteDisabled,
    setBtnVoteDisabled,
    setDetailVoteDialogData,
    isConnected,
    openCancelDialog,
    setChoices,
    initBalance,
    symbol,
  } = props;

  const [balance, setBalance] = useState(initBalance);
  const [voteMode, setVoteMode] = useState(isVoteMode);

  const max = choices[0].progress;
  const hasItems = choices && choices.length > 0;
  const save = async () => {
    openVoteDialog();
  };

  const resetBalance = () => {
    setBalance(initBalance);
  };

  const handleCancel = () => {
    openCancelDialog(resetBalance);
  };

  useEffect(() => {
    setVoteMode(isVoteMode);
  }, [isVoteMode]);

  const calculateBalance = (id, evt) => {
    let item = choices.find((c) => c.choice.id === id);
    let index = choices.findIndex((c) => c.choice.id === id);
    let value = +evt.target.value ?? 0;

    const maxForThisInput = Math.floor(
      Math.sqrt(
        initBalance -
          choices
            .filter((x) => x.choice.id !== id)
            .reduce((a, b) => a + Math.pow(b.value, 2), 0)
      )
    );

    value = value > maxForThisInput ? maxForThisInput : value;

    if (item) {
      let editChoice = {
        i: item.i,
        choice: item.choice,
        value: value,
        progress: item.progress,
      };

      choices.splice(index, 1);
      choices.splice(index, 0, editChoice);
      setChoices(choices);
    }

    const calc = choices.reduce((a, b) => a + Math.pow(b.value, 2), 0);
    const balance = initBalance - calc;

    if (initBalance < calc || +evt.target.value > maxForThisInput) {
      evt.target.max = maxForThisInput;
      evt.target.value = maxForThisInput;
      setBtnVoteDisabled(false);
    } else {
      if (balance === initBalance) {
        setBtnVoteDisabled(true);
      } else {
        evt.target.value = value;
        setBtnVoteDisabled(false);
      }
    }
    setBalance(balance);
  };

  return (
    <div className="vote-form">
      <div className="p-4">
        {voteMode && (
          <div className="vote-form-progress-bar mb-2">
            <span className="vote-form-progress-bar-label">
              {isConnected ? (
                <span>
                  {formatBalanceValue(balance)}
                  {symbol}
                  {` Left`}
                </span>
              ) : (
                <span>Connect your wallet</span>
              )}
            </span>
            <ProgressBar value={balance} max={initBalance} className="mb-3" />
          </div>
        )}
        <div className="vote-content">
          {hasItems ? (
            <>
              {choices.map((item, i) => (
                <div
                  key={`choice${item.choice.id}`}
                  className={`asset-list-item ${
                    item.progress === 0 ? `empty` : ``
                  }`}
                >
                  <div
                    className="clearfix asset-list-item-volume"
                    key={`choice-progress${item.choice.id}`}
                  >
                    <ProgressBar value={item.progress} max={max} />
                  </div>
                  <div className="clearfix vote-info">
                    <div
                      className="col-6 float-left px-1"
                      onClick={() => {
                        if (item.progress > 0)
                          setDetailVoteDialogData(item.choice.id);
                        else return false;
                      }}
                    >
                      <b>{item.choice.value}</b>
                    </div>
                    <div className="col-6 px-1">
                      <fieldset className={"asset-list-item-weight editable"}>
                        <legend>$Vote</legend>
                        <input
                          className="from-control"
                          disabled={!voteMode}
                          type="number"
                          min={0}
                          step={1}
                          pattern="[0-9]*"
                          placeholder="0"
                          onChange={(evt) =>
                            calculateBalance(item.choice.id, evt)
                          }
                          {...(!voteMode
                            ? { value: item.progress }
                            : {
                                value: item.value > 0 ? item.value : ``,
                              })}
                        />
                      </fieldset>
                    </div>
                  </div>
                </div>
              ))}
            </>
          ) : (
            <div>List is empty</div>
          )}
        </div>

        {voteMode && (
          <div className="vote-form-footer">
            <div className="vote-form-footer-action">
              <Button
                disabled={btnVoteDisabled}
                onClick={() => save()}
                className="vote-button"
              >
                Vote
              </Button>
            </div>
            <div className="vote-form-footer-info">
              <Button
                disabled={btnVoteDisabled}
                className="vote-cancel-button"
                onClick={handleCancel}
              >
                Cancel vote
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default SemanticBallot;
