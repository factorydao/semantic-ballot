import React from "react";
import "./Button.scss";

const Button = (props) => {
  const { type, onClick, disabled, className, loading } = props;
  const classNameResult = className ? className : "";
  return (
    <button
      type={type || "button"}
      onClick={onClick}
      className={`${classNameResult} button`}
      disabled={disabled || false}
    >
      {!loading && props.children}
    </button>
  );
};

export default Button;
