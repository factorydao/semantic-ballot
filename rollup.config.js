import babel from "@rollup/plugin-babel";
import resolve from "@rollup/plugin-node-resolve";
import peerDepsExternal from "rollup-plugin-peer-deps-external";
import commonjs from "@rollup/plugin-commonjs";
import sass from "node-sass";
import postcss from "rollup-plugin-postcss";
import autoprefixer from "autoprefixer";
import pkg from "./package.json";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  input: pkg.source,
  output: [{ file: pkg.main, format: "cjs", sourcemap: true }],
  plugins: [
    peerDepsExternal(),
    commonjs({
      include: "node_modules/**",
      namedExports: {
        "react-is": ["isForwardRef", "isValidElementType"],
      },
    }),
    resolve(),
    babel({
      exclude: "node_modules/**",
      babelHelpers: "bundled",
    }),
    postcss({
      preprocessor: (content, id) =>
        new Promise((res) => {
          const result = sass.renderSync({ file: id });

          res({ code: result.css.toString() });
        }),
      plugins: [autoprefixer],
      modules: {
        scopeBehaviour: "global",
      },
      sourceMap: true,
      extract: "sematic-ballot.css",
    }),
  ],
};
